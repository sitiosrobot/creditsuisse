﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization.Json;
using banxico.Entity;

namespace banxico
{
    public class SIERest
    {
        public static List<DataPoint> ReadSerie(string serie, string url, string token)
        {
            List<DataPoint> responseData = new List<DataPoint>();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                string urlbanxico = string.Format(url, serie);
                HttpWebRequest request = WebRequest.Create(urlbanxico) as HttpWebRequest;
                request.Accept = "application/json";
                request.Headers["Bmx-Token"] = token;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));

                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                Response jsonResponse = objResponse as Response;

                if (jsonResponse.seriesResponse.series.Length > 0)
                {
                    Serie serieObj = jsonResponse.seriesResponse.series[0];

                    foreach (DataSerie dataSerie in serieObj.Data)
                    {
                        if (dataSerie.DataString.Equals("N/E")) continue;
                        responseData.Add(new DataPoint(dataSerie.Data, dataSerie.DateString));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return responseData;
        }
    }
}
