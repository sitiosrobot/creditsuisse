﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Data;
using System.Collections.Generic;

namespace CreditSuisse.Areas.banxico.Controllers
{
    public class DBConnection : IDisposable
    {
        private List<SqlParameter> Parameters = null;
        private static SqlConnection conn = null;

        public DBConnection()
        {
            if (conn == null)
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VolarisAuditDB"].ConnectionString);
            conn.Open();
        }

        public void ExecuteQuery(string query)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                    cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public DataTable ExecuteQueryToDataTable(string query)
        {
            DataTable table = null;

            try
            {
                using (SqlDataAdapter da = new SqlDataAdapter(query, conn))
                {
                    table = new DataTable();
                    da.Fill(table);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return table;
        }

        public T ExecuteQueryToValue<T>(string query)
        {
            T result = default(T);

            try
            {
                DataTable table = ExecuteQueryToDataTable(query);

                if (table.Rows.Count > 0)
                    result = (T)table.Rows[0].ItemArray[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return result;
        }

        public void ExecuteStoredProcedure(string sp)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand(sp, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddRange(Parameters.ToArray());
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public DataTable ExecuteStoredProcedureToDataTable(string sp)
        {
            DataTable table = null;

            try
            {
                using (SqlCommand cmd = new SqlCommand(sp, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddRange(Parameters.ToArray());

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        table = new DataTable();
                        da.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return table;
        }

        public T ExecuteStoredProcedureToValue<T>(string sp)
        {
            T result = default(T);

            try
            {
                DataTable table = ExecuteStoredProcedureToDataTable(sp);

                if (table.Rows.Count > 0)
                    result = (T)table.Rows[0].ItemArray[0];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }

            return result;
        }

        public void AddParameter(string name, object value)
        {
            if (Parameters == null)
                Parameters = new List<SqlParameter>();

            Parameters.Add(new SqlParameter(name, value));
        }

        public void ClearParameters()
        {
            Parameters = null;
        }

        public void Close()
        {
            if (conn != null)
                conn.Close();
        }

        public void Dispose()
        {
            Close();
        }
    }
}