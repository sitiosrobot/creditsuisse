﻿using System;
using banxico;
using banxico.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestCreditSuisse
{
    [TestClass]
    public class UTBanxico
    {
        [TestMethod]
        public void TestMethod1()
        {
            var response = SIERest.ReadSerie("SP74665", "https://www.banxico.org.mx/SieAPIRest/service/v1/series/{0}/datos", "f33522b9c564fdee11e84c6371c31eb117491f1c6f60c194b99705e3d5c2eb72");
            Assert.IsNotNull(response);
        }
    }
}
