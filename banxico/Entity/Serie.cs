﻿using System;
using System.Runtime.Serialization;

namespace banxico.Entity
{
    [DataContract]
    public class Serie
    {
        [DataMember(Name = "titulo")]
        public string Title { get; set; }

        [DataMember(Name = "idSerie")]
        public string IdSeria { get; set; }

        [DataMember(Name = "datos")]
        public DataSerie[] Data { get; set; }
    }

    [DataContract]
    public class DataSerie
    {
        [DataMember(Name = "fecha")]
        public string DateString { get; set; }

        [DataMember(Name = "dato")]
        public string DataString { get; set; }

        public DateTime Date { get { return DateTime.Parse(this.DateString); } }

        public double Data { get { return double.Parse(this.DataString); } }
    }

    [DataContract]
    public class SerieResponse
    {
        [DataMember(Name = "series")]
        public Serie[] series { get; set; }
    }

    [DataContract]
    public class Response
    {
        [DataMember(Name = "bmx")]
        public SerieResponse seriesResponse { get; set; }
    }
}
