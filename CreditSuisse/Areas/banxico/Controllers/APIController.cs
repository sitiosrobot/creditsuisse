﻿using banxico;
using banxico.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;

namespace CreditSuisse.Areas.banxico.Controllers
{
    public class APIController : Controller
    {
        JsonSerializerSettings jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        // GET: banxico/API/SIE/SP74665
        public ActionResult SIE(string serie)
        {
            ViewBag.Serie = serie;
            string url = ConfigurationManager.AppSettings["Url"];
            string token = ConfigurationManager.AppSettings["Token"];
            List<DataPoint> data = SIERest.ReadSerie(serie, url, token);
            ViewBag.DataPoints = JsonConvert.SerializeObject(data, jsonSetting);

            if (data.Count > 0)
                return View();
            else
                return RedirectToAction("Index", "Home", new { Area = "" });
        }
    }
}
