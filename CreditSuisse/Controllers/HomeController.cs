﻿using CreditSuisse.Areas.banxico.Controllers;
using CreditSuisse.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CreditSuisse.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["UserName"] != null)
                return RedirectToAction("Home");

            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel loginDataModel)
        {
            if (Session["UserName"] != null)
                return RedirectToAction("Home");

            if (ModelState.IsValid)
            {
                if (UserValidation(loginDataModel))
                {
                    return RedirectToAction("Home");
                }
            }

            return View(loginDataModel);
        }

        public ActionResult Logout()
        {
            if (Session["UserName"] != null)
                Session["UserName"] = null;

            return RedirectToAction("Home");
        }

        public ActionResult Home()
        {
            return View();
        }

        private bool UserValidation(LoginViewModel loginDataModel)
        {
            string cnnString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            bool ValidateUser = false;
            using (DBConnection DB = new DBConnection())
            {
                DB.ClearParameters();
                DB.AddParameter("@User", loginDataModel.User);
                DB.AddParameter("@Password", loginDataModel.Password);
                ValidateUser = DB.ExecuteStoredProcedureToValue<bool>("[dbo].[sp_validateUser]");
            }
                
            if (ValidateUser)
            {
                Session["UserName"] = loginDataModel.User;
                return true;
            }

            return false;
        }
    }
}