﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CreditSuisse.Models
{
    public class LoginViewModel
    {
        [Display(Name = "Username")]
        [Required(ErrorMessage = "This field is required.")]
        [StringLength(30, ErrorMessage = "Length between 5 and 30 characters.", MinimumLength = 5)]
        [DataType(DataType.Text)]
        public string User { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "This field is required.")]
        [StringLength(15, ErrorMessage = "Length between 5 and 15 characters.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Rememberme")]
        public bool RememberMe { get; set; }
    }
}