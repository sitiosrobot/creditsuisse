USE [CreditSuisse]
GO

IF OBJECT_ID('[dbo].[Users]', 'U') IS NOT NULL
DROP TABLE [dbo].[Users]
GO

CREATE TABLE [dbo].[Users]
(
    [Id] INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
    [User] NVARCHAR(50) NOT NULL,
    [Password] NVARCHAR(50) NOT NULL
);
GO

INSERT INTO [dbo].[Users] ([User],[Password]]) VALUES ('emmanuel','ricoaguilar')
GO

IF OBJECT_ID('[dbo].[sp_validateUser]', 'P') IS NOT NULL
DROP PROC [dbo].[sp_validateUser]
GO

CREATE PROCEDURE [dbo].[sp_validateUser] 
    @User NVARCHAR(50),
	@Password NVARCHAR(50)
AS
BEGIN
    IF EXISTS (SELECT 1 FROM [dbo].[Users] WHERE [User] = @User AND [Password] = @Password)
        SELECT 1 AS [Responce];
	ELSE
		SELECT 0 AS [Responce];
END
GO
